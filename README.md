# lm2europresse

User script pour passer rapidement d'un article du Monde à la version sur Europresse de la bibliothèque Diderot.

Installation : installer [Tampermonkey](https://www.tampermonkey.net/) et visiter le lien suivant :

https://gitlab.com/juba/lm2europresse/-/raw/master/lm2europresse.user.js

Usage : faire `Alt+g` sur une page du site du Monde.

Attention, ne fonctionne pas avec ViolentMonkey, mais marche avec TamperMonkey.