// ==UserScript==
// @name        Le Monde -> Europresse
// @namespace   https://nozav.org/
// @include     http*://*.lemonde.fr/*
// @include     http*://*europresse*bibliotheque-diderot.fr/*
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_deleteValue
// @downloadURL https://gitlab.com/juba/lm2europresse/-/raw/master/lm2europresse.user.js
// @version     0.0.2
// @author      juba
// @description 1/21/2021, 5:35:53 PM
// ==/UserScript==


function doc_keyUp(e) {
    switch (e.keyCode) {
        case 71:
            //g
            if (e.altKey) {
        		  let title = document.title;
              GM_setValue("title", title);
          	  window.location = "http://acces.bibliotheque-diderot.fr/login?url=https://nouveau.europresse.com/access/ip/default.aspx?un=ENSLYONT_1";
            }
        break;
    }
}

if (window.location.href.indexOf("lemonde") != -1 ) {
  document.addEventListener('keyup', doc_keyUp, false);
}

if (window.location.href.indexOf("diderot.fr/Search/Reading") != -1 ) {
  let title = GM_getValue("title", false);
  GM_deleteValue("title");
  if (title) {
      title = "TIT_HEAD=" + title;
      document.getElementById("Keywords").value = title;
      document.querySelector("#main form").submit();
  }
}

